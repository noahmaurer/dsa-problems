# DATE

# Difficulty

Easy

My thoughts:

-   Easy

## [####. PROBLEM_NAME](PROBLEM_URL)

### Problem

Copy-paste the problem here

#### Example 1

```
PUT EXAMPLE HERE
```

#### Example 2

```
PUT EXAMPLE HERE
```

#### Constraints

-   `CONSTRAINT1`
-   `CONSTRAINT2`

### Preliminary Solution

-   **Time Complexity**: O(TIME)
-   **Space Complexity**: O(SPACE)

#### Initial Thoughts

Put some initial thoughts. How might you approach this?

#### Solution Code

```
SOLUTION CODE GOES HERE
```

#### Notes

-   Anything additional goes here in bullets

### [Best Available Solution](SOLUTION_LINK)

-   **Time Complexity**: O(TIME)
-   **Space Complexity**: O(SPACE)

#### _Backside_

Put a small compact description of the solution that can fit on the back of a flashcard here. It should describe the best approach you can find on the internet and is an improvement over the preliminary solution.

#### Additional Details

Put any additional details or detailed description of the solution here. Not meant to go on a flashcard, but should be something you refer to in the case that you don't understand your compact descriptions.

#### Solution Code

```
SOLUTION CODE GOES HERE
```

#### Notes

-   Anything additional goes here in bullets
